#! /usr/bin/env python
# -*- coding: utf-8; -*-

# This pydoc element is sent as long_description to setup and should
# be re structured text. See:
# http://docutils.sourceforge.net/docs/user/rst/quickstart.html
"""Liu logchecker.

Liu logchecker examines the logs that accumulate on your log server
and sends email to your administrators about the interesting log
messages.
"""

# Copyright (C) 2014 Linköpings universitet

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

__author__ = """Kent Engström <kent@nsc.liu.se>
Ulrik Haugen <qha@lysator.liu.se>"""


import os
import sys
from distutils.core import setup
from distutils.command.build import build
from distutils.command.install import install
from distutils.command.sdist import sdist
from subprocess import call, Popen, PIPE

assert sys.hexversion >= 0x02060000, "Install Python 2.6 or greater"


# Many apologies:
class build_catfrom (build):
    def run(self):
        def call_compiler(infiles, outfile):
            call([ 'cc',
                   '-Wall',
                   '-O3',
                   '-o', outfile, ]
                 + infiles)

        # Run stock build code
        build.run(self)
        # and custom build code
        self.mkpath(self.build_temp)
        infiles = [ 'catfrom.c', ]
        outfile = os.path.join(self.build_temp, 'catfrom')

        self.make_file(infiles, outfile,
                       call_compiler, [ infiles, outfile, ])


# Many apologies:
class install_catfrom (install):
    def initialize_options (self):
        install.initialize_options(self)
        self.build_temp = None

    def finalize_options (self):
        install.finalize_options(self)
        self.set_undefined_options('build', ('build_temp', 'build_temp'))

    def run(self):
        # Run stock install code
        install.run(self)
        # and custom install code
        self.copy_tree(self.build_temp, self.install_scripts)

    def get_outputs(self):
        outputs = install.get_outputs(self)
        outputs.extend([ os.path.join(self.install_scripts, fn)
                         for fn in os.listdir(self.build_temp) ])
        return outputs


# Version numbers come from git/source distributions to setup.py with
# get_version(), from there to spec files via standard bdist_rpm and
# from setup.py to source distributions with the custom sdist_version
# class.

version_fn = "version.txt"

def get_version():
    """Return the version number of Liu logchecker."""
    if os.path.exists(version_fn):
        with open(version_fn) as version_info:
            return version_info.readline().rstrip()
    else:
        return Popen('git describe --dirty'.split(),
                     stdout=PIPE).communicate()[0].rstrip().lstrip('version-').replace('-', '.')


class sdist_version (sdist):
    def make_release_tree(self, base_dir, files):
        # Run stock sdist code
        sdist.make_release_tree(self, base_dir, files)
        # and write version.txt
        version = get_version()
        with open(os.path.join(base_dir, version_fn), 'w') as version_dest:
            version_dest.write(version + '\n')


setup(
    # Metadata:
    name = "liu-logchecker",
    version = get_version(),
    description = "Liu logchecker",
    author = "Kent Engström, Ulrik Haugen",
    author_email = "qha@lysator.liu.se",
    url = "http://www.lysator.liu.se/~qha/liu-logchecker",
    license = "GPLv2",
    keywords = ('syslog', 'audit'),
    long_description = __doc__,
    # Python scripts:
    scripts = [ "logchecker_check_host",
                "logchecker_check_hosts",
                "logchecker_test_config",
               ],
    py_modules = [ "logchecker_common" ],
    # version.txt and catfrom:
    cmdclass = { 'sdist': sdist_version,
                 'build': build_catfrom,
                 'install': install_catfrom, }
    )
