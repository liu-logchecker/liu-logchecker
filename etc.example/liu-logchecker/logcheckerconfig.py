# Keep this file compatible with both Python and Bourne shell!

sender='Log Checker <logchecker@example.com>'
default_recipient='No m-lines <logchecker@example.com>'

# Limit reports to 10 MB:
message_size_limit=10000000

# base_path is used by logchecker_check_hosts to find configuration
# files, in base_path/conf/*.conf
base_path='/opt/example/liu-logchecker'

# log_template will be subject to (in order):
# * strftime substitution
# * percent formatting with a hash containing 'host'
# * globbing in logs_path or base_path/logs if logs_path is empty
log_template='%Y-%m-%d/%%(host)s/*.log'

# Optional:
logs_path=''
state_path=''
temp_path=''
