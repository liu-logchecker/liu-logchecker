# -*- coding: utf-8; -*-

# Copyright (C) 2005, 2007, 2014 Linköpings universitet

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.

__author__ = '''Ulrik Haugen <ulrik.haugen@liu.se> (2006-2011, 2014)'''


import re


def parse_template_invocation(label_string):
    """Extract template labels in /label_string/ (from a template
    inclusion command). Return labels in a dict."""

    label_list = re.split(r'[ \t]([_a-z][_a-z0-9]*)=',
                          " " + label_string)

    # The first element in label_list will always be the empty
    # string appearing before the first 'identifier=' occurence:
    if label_list[0] != '':
        raise ValueError, ("not a label=value list: '%s'"
                           % label_list[0])

    template_labels = {}
    for ii in range(1, len(label_list), 2):
        if label_list[ii] in template_labels:
            raise ValueError, ("key '%s' occurs more than once"
                               % label_list[ii])
        template_labels[label_list[ii]] = label_list[ii + 1]

    return template_labels
