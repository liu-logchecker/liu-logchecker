/*  catfrom.c

    Copyright (C) 2000 Kent Engström, UNIT

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

#include <stdio.h>
#include <stdlib.h>

#define BUFLEN (8*1024)
char buffer[BUFLEN];

int main(int argc, char **argv)
{
  long offset, newoffset;
  FILE *f;

  if (argc != 3)
  {
    fprintf(stderr, "ERROR\n");
    exit(1);
  }

  offset = atol(argv[1]);
  
  if ((f = fopen(argv[2], "r")) == NULL)
  {
    fprintf(stderr, "ERROR\n");
    exit(2);
  }

  fseek(f, offset, SEEK_SET);
  while (fgets(buffer, BUFLEN, f) != NULL)
  {
    fputs(buffer, stdout);
  }
  newoffset = ftell(f);
  fprintf(stderr, "%ld\n", newoffset);
  exit(0);
}
